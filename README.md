<img src="http://vignette2.wikia.nocookie.net/logopedia/images/a/a8/Marvel-logo.png/revision/latest?cb=20140701230531" width="1024" height="180">

Marveldex
==

_Marveldex is a job opportunity test for H19. It's an app similiar to Pokedex :P_

_Using this application, users will be able to browse through the Marvel library of characters. The data is available by connecting to the [Marvel API](http://developer.marvel.com/)._

## Getting Started

This project uses [Cocoapods](https://cocoapods.org/) as dependency manager.

```
$ pod install
$ open marveldex.xcworkspace
```

* Cocoapod version: `1.0.0.rc.1`
* You must have previous configured ruby environment
