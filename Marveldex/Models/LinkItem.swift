//
//  LinkItem.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import SwiftyJSON

struct LinkItem: JSONDeserialization {
    var type: String
    var url: NSURL

    init(json: JSON) {
        type = json["type"].string ?? ""
        url = NSURL(string: json["url"].string!)!
    }
}