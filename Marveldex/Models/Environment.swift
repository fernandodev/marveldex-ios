//
//  Environment.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/1/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Plist

struct Environment {
    static var plist = Plist(
        path: NSBundle.mainBundle().pathForResource("Environment", ofType: "plist")!
    )
    
    static var marvelAPI = plist["Marvel API"].string ?? ""    
    static var marvelPublicKey = plist["Marvel Public Key"].string ?? ""    
}
