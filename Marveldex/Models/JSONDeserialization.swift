//
//  JSONDeserialization.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol JSONDeserialization {
    init(json: JSON)
}
