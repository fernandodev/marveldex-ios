//
//  MagazineItem.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import SwiftyJSON

struct MagazineItem: JSONDeserialization {
    var title: String
    var thumbnailURL: NSURL

    init(json: JSON) {
        let thumbnailPath = "\(json["thumbnail"]["path"]).\(json["thumbnail"]["extension"])"
        
        title = json["title"].string ?? ""
        thumbnailURL = NSURL(string: thumbnailPath)!
    }
}