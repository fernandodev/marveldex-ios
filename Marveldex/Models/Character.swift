//
//  Character.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Character: JSONDeserialization {

    var id: Int
    var name: String
    var charDescription: String
    var thumbnailURL: NSURL
    var isComicsAvailable: Bool = false
    var isSeriesAvailable: Bool = false
    var isStoriesAvailable: Bool = false
    var isEventsAvailable: Bool = false
    var links: [LinkItem]

    init(json: JSON) {
        let thumbnailPath = "\(json["thumbnail"]["path"]).\(json["thumbnail"]["extension"])"

        id = json["id"].int ?? 0
        name = json["name"].string ?? ""
        charDescription = json["description"].string ?? ""
        thumbnailURL = NSURL(string: thumbnailPath)!
        links = json["urls"].ext_transformJsonArray()
        if let comics = json["comics"]["available"].int {
            isComicsAvailable = comics > 0
        }
        if let series = json["series"]["available"].int {
            isSeriesAvailable = series > 0
        }
        if let stories = json["stories"]["available"].int {
            isStoriesAvailable = stories > 0
        }
        if let events = json["events"]["available"].int {
            isEventsAvailable = events > 0
        }
    }
}
