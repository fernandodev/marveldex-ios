//
//  Dictionary.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/1/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation

extension Dictionary {
    func merge(dict: Dictionary?) -> Dictionary {
        var result = self
        if let dict = dict {
            for (key, value) in dict {
                result[key] = value
            }
        }
        
        return result
    }
}