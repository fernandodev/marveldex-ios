//
//  String.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/1/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation

extension String {
    //Easy MD5 conversion for strings
    //Thanks https://gist.github.com/finder39/f6d71f03661f7147547d
    //
    func md5() -> String {
        let str = self.cStringUsingEncoding(NSUTF8StringEncoding)
        let strLen = CUnsignedInt(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.dealloc(digestLen)
        
        return String(format: hash as String)
    }
}
