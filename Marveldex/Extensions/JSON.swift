//
//  JSON.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import SwiftyJSON

extension JSON {

    func ext_transformJson<T: JSONDeserialization>() -> T {
        return T(json: self)
    }

    func ext_transformJsonArray<T: JSONDeserialization>() -> [T] {
        var array: [T] = []

        assert(self.type == .Array)

        for index in 0...(self.count-1) {
            let e = self[index]
            array.append(T(json: e))
        }

        return array
    }
}
