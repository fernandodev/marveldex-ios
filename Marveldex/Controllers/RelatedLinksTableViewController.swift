//
//  RelatedLinksTableViewController.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

class RelatedLinksTableViewController: UITableViewController {

    var links: [LinkItem] = []

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        refresh()
    }

    func refresh() {
        tableView.reloadData()
    }

    //MARK: Table View Data
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? links.count : 0
    }

    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRectMake(8, 5, 200, 21))
        label.textColor = UIColor.redColor()
        label.font = UIFont.boldSystemFontOfSize(12)
        label.text = "RELATED LINKS"

        let view = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 30))
        view.backgroundColor = UIColor.clearColor()

        view.addSubview(label)

        return view
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("LinkCell")!
        let type = links[indexPath.row].type ?? ""

        cell.textLabel?.text = type

        return cell
    }

    //MARK: Actions
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let link = links[indexPath.row]
        if UIApplication.sharedApplication().canOpenURL(link.url) {
            UIApplication.sharedApplication().openURL(link.url)
        }
    }
}