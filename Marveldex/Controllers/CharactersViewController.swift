//
//  CharactersViewController.swift
//  marveldex
//
//  Created by Fernando Martinez on 4/30/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import UIKit
import Plist
import Alamofire
import SwiftyJSON

class CharactersViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    let api = APICharacters()
    var refreshControl: UIRefreshControl!
    var characters: [Character] = []
    var selectedChar: Character?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        load()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupViews() {
        refreshControl = UIRefreshControl()
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(load), forControlEvents: .ValueChanged)
    }

    //MARK: API Load
    func load() {
        refreshControl.beginRefreshing()
        api.characters(
            0,
            success: { results in
                self.characters = results
                self.tableView.reloadData()
            }, failure: { error in

            }, completion: {
                self.refreshControl.endRefreshing()
            }
        )
    }

    //MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        assert(selectedChar != nil)
        if let id = segue.identifier where id == "show-character" {
            if let vc = segue.destinationViewController as? CharacterViewController {
                vc.character = selectedChar
            }
        }

        super.prepareForSegue(segue, sender: sender)
    }
}

//MARK: UITableViewDataSource
extension CharactersViewController: UITableViewDataSource {

    //MARK: Quantity and Sizing
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.section == 0 ? 160 : 0
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? characters.count : 0
    }

    //MARK: Cells
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("CharCell") as! CharacterViewCell
        let character = characters[indexPath.row]

        cell.fillWithThumbnailURL(character.thumbnailURL, andName: character.name)

        return cell
    }
}

//MARK: UITableViewDelegate
extension CharactersViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedChar = characters[indexPath.row]
        performSegueWithIdentifier("show-character", sender: self)
    }
}

