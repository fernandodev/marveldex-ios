//
//  MagazinesCollectionViewController.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

//TODO: It lacks empty list state design
class MagazinesViewController: UIViewController {
    @IBOutlet weak var sectionNameLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!

    let api = APICharacters()
    var character: Character!
    var mediaType: APICharacters.MediaType = .Comics
    var items: [MagazineItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.indicatorStyle = .White
        sectionNameLabel.text = "\(mediaType)".uppercaseString
    }

    override func viewWillAppear(animated: Bool) {
        refresh()
        super.viewWillAppear(animated)
    }

    func refresh() {
        if mediaType == .Comics && !character.isComicsAvailable { return }
        else if mediaType == .Series && !character.isSeriesAvailable { return }
        else if mediaType == .Stories && !character.isStoriesAvailable { return }
        else if mediaType == .Events && !character.isEventsAvailable { return }

        api.media(
            character.id,
            mediaType: mediaType,
            success: { comics in
                self.items = comics
                self.collectionView.reloadData()
            },
            failure: { _ in

            },
            completion: {

            }
        )
    }
}

//MARK: UICollectionViewDataSource
extension MagazinesViewController: UICollectionViewDataSource {

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? items.count : 0
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("MagazineCell", forIndexPath: indexPath)

        let item = items[indexPath.row]
        if let cell = cell as? MagazineViewCell {
            cell.fillWithThumbnailURL(item.thumbnailURL, andName: item.title)
        }

        return cell
    }
}

//MARK: UICollectionViewDelegate
extension MagazinesViewController: UICollectionViewDelegate {
    //TODO: show image
}