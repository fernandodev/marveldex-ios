//
//  CharacterViewController.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit
import PureLayout

class CharacterViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dynamicContentView: UIView!

    var api = APICharacters()
    var character: Character!
    var didSetupConstraints: Bool = false
    var preferredDynamicContentHeight: CGFloat = 0

    var comicsViewController: MagazinesViewController!
    var seriesViewController: MagazinesViewController!
    var storiesViewController: MagazinesViewController!
    var eventsViewController: MagazinesViewController!
    var relatedLinksViewController: RelatedLinksTableViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    override func updateViewConstraints() {
        if didSetupConstraints {
            dynamicContentView.constraints.first?.constant = preferredDynamicContentHeight
            didSetupConstraints = false
        }
        super.updateViewConstraints()
    }

    //MARK: Setup Views
    func setupViews() {
        navigationItem.title = character.name
        coverImageView.af_setImageWithURL(character.thumbnailURL)
        nameLabel.text = character.name
        descriptionLabel.text = character.charDescription
        descriptionLabel.sizeToFit()
        setupMediaItemViews()
    }

    func setupMediaItemViews() {
        if let storyboard = storyboard {
            comicsViewController = storyboard.instantiateViewControllerWithIdentifier("CharacterItemsList") as! MagazinesViewController
            seriesViewController = storyboard.instantiateViewControllerWithIdentifier("CharacterItemsList") as! MagazinesViewController
            storiesViewController = storyboard.instantiateViewControllerWithIdentifier("CharacterItemsList") as! MagazinesViewController
            eventsViewController = storyboard.instantiateViewControllerWithIdentifier("CharacterItemsList") as! MagazinesViewController

            relatedLinksViewController = storyboard.instantiateViewControllerWithIdentifier("RelatedLinks") as! RelatedLinksTableViewController

            comicsViewController.character = character
            seriesViewController.character = character
            storiesViewController.character = character
            eventsViewController.character = character

            comicsViewController.mediaType = .Comics
            seriesViewController.mediaType = .Series
            storiesViewController.mediaType = .Stories
            eventsViewController.mediaType = .Events

            relatedLinksViewController.links = character.links

            addChildViewController(comicsViewController)
            addChildViewController(seriesViewController)
            addChildViewController(storiesViewController)
            addChildViewController(eventsViewController)
            addChildViewController(relatedLinksViewController)

            dynamicContentView.addSubview(comicsViewController.view)
            dynamicContentView.addSubview(seriesViewController.view)
            dynamicContentView.addSubview(storiesViewController.view)
            dynamicContentView.addSubview(eventsViewController.view)
            dynamicContentView.addSubview(relatedLinksViewController.view)

            comicsViewController.view.autoSetDimension(.Height, toSize: 210, relation: .Equal)

            comicsViewController.view.autoPinEdge(.Top, toEdge: .Top, ofView: dynamicContentView)
            comicsViewController.view.autoPinEdgeToSuperviewEdge(.Left, withInset: 10)
            comicsViewController.view.autoPinEdgeToSuperviewEdge(.Right, withInset: 10)

            setupMediaViewConstraints(seriesViewController.view, withTopView: comicsViewController.view, toHeight: 210, withEdgeInset: 10)
            setupMediaViewConstraints(storiesViewController.view, withTopView: seriesViewController.view, toHeight: 210, withEdgeInset: 10)
            setupMediaViewConstraints(eventsViewController.view, withTopView: storiesViewController.view, toHeight: 210, withEdgeInset: 10)
            setupMediaViewConstraints(relatedLinksViewController.view, withTopView: eventsViewController.view, toHeight: 200)

            relatedLinksViewController.didMoveToParentViewController(self)
            comicsViewController.didMoveToParentViewController(self)
            seriesViewController.didMoveToParentViewController(self)
            storiesViewController.didMoveToParentViewController(self)
            eventsViewController.didMoveToParentViewController(self)

            preferredDynamicContentHeight = 1030
            didSetupConstraints = true
        }
    }

    func setupMediaViewConstraints(view: UIView, withTopView topView: UIView, toHeight height: CGFloat, withEdgeInset inset: CGFloat = 0) {
        view.autoSetDimension(.Height, toSize: height, relation: .Equal)
        view.autoPinEdge(.Top, toEdge: .Bottom, ofView: topView)
        view.autoPinEdgeToSuperviewEdge(.Left, withInset:  inset)
        view.autoPinEdgeToSuperviewEdge(.Right, withInset: inset)
    }
}