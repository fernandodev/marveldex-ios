//
//  CharacterViewCell.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class CharacterViewCell: UITableViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    func fillWithThumbnailURL(thumbnailURL: NSURL, andName name: String) {
        let maskImageView = UIImageView(image: UIImage(named: "bg-cell-title"))
        maskImageView.frame = CGRectMake(0, 0, 150, 30)        

        thumbnailImageView.image = nil
        thumbnailImageView.af_setImageWithURL(thumbnailURL)
        nameLabel.text = name
        nameLabel.maskView = maskImageView
    }
}