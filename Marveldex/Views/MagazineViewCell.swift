//
//  MagazineCell.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/3/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

class MagazineViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    func fillWithThumbnailURL(thumbnailURL: NSURL, andName name: String) {
        thumbnailImageView.image = nil
        thumbnailImageView.af_setImageWithURL(thumbnailURL)
        nameLabel.text = name
    }
}
