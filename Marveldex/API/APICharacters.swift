//
//  APICharacters.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/2/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import SwiftyJSON
import Alamofire
import Foundation

class APICharacters: APIClient {
    enum MediaType: String {
        case Comics = "comics"
        case Series = "series"
        case Stories = "stories"
        case Events = "events"
    }
    func characters(offset: Int, success: ((result: [Character]) -> Void), failure: (error: APIError) -> Void, completion: () -> Void) {

        get(
            "v1/public/characters",
            params: [
                "offset": offset,
                "limit": 5
            ],
            success: { json in
                let results = json["data"]["results"]
                let characters: [Character] = results.ext_transformJsonArray()
                success(result: characters)
            },
            failure: failure,
            completion: completion
        )
    }

    func media(characterId: Int, mediaType: MediaType, success: ((result: [MagazineItem]) -> Void), failure: (error: APIError) -> Void, completion: () -> Void) {

        get(
            "v1/public/characters/\(characterId)/\(mediaType.rawValue)",
            params: nil,
            success: { json in
                let results = json["data"]["results"]
                let items: [MagazineItem] = results.ext_transformJsonArray()
                success(result: items)
            },
            failure: failure,
            completion: completion
        )
    }
}