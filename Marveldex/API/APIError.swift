//
//  APIError.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/1/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation

class APIError: NSObject {
    static var InvalidRequest = Int.max

    var code: Int
    var message: String {
        switch code {
        case 401:
            return "Invalid credentials"
        default:
            return "Invalid request"
        }
    }
    override var description: String {
        return message
    }
    
    init(code: Int) {
        self.code = code
    }
}