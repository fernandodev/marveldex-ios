//
//  APIClient.swift
//  Marveldex
//
//  Created by Fernando Martinez on 5/1/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Alamofire
import SwiftyJSON

class APIClient {
    private var requestTimestamp: String {        
        return String(format: "%i", (NSDate().timeIntervalSince1970 * 1000))
    }
    private var authenticationParams: [String:AnyObject] {
        let timestamp = requestTimestamp

        return [
            "ts": timestamp,
            "apikey": Environment.marvelPublicKey,
            "hash": calculateRequestHash(timestamp)
        ]
    }
    
    //MARK: GET
    func get(path: String, params: [String:AnyObject]?, success: ((result: JSON) -> Void), failure: (error: APIError) -> Void, completion: () -> Void) {
        let parameters = authenticationParams.merge(params)
        Alamofire.request(.GET, urlForPath(path), parameters: parameters).responseJSON { response in
            switch response.result {
            case .Success:
                if let body = response.response, json = response.result.value where body.statusCode == 200 {
                    success(result: JSON(json))
                } else {
                    failure(error: APIError(code: response.response?.statusCode ?? APIError.InvalidRequest))
                }
                break
            case .Failure:
                //TODO: handle possible failures
                failure(error: APIError(code: APIError.InvalidRequest))
                break
            }
            completion()
        }
    }
    
    //MARK: Path and Authentication Utils
    private func calculateRequestHash(timestamp: String) -> String {
        //Private keys mustn't be declared in Environment.plist
        //Use it only for hash calculation
        //
        // Actually private keys mustn't be used in Client side applications ... but
        // Marvel's API needs this calculation... It's a bad API design.
        //
        return (timestamp + "4110ff13517bd1c6c91820f27f2f13bb2f1e5939" + Environment.marvelPublicKey).md5()
    }
    
    private func urlForPath(path: String) -> String {
        return Environment.marvelAPI + path
    }
}